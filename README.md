/users/123/details {r/w user only}

/users/123/servers {r/w user only}
    /...

/servers
    /server_123
        /details
            :name, :thumb
        /members
            /user_abc: 'owner'
            /user_def: 'chatter'
        /threads
            /thread_123
                :author, :text, :timestamp
                    /embed
                        :title, :type, :description, :url, :thumb
        /posts
            /thread_123
                /post_1
                    :author, :text, :timestamp
                    /embed
                        :title, :type, :description, :url, :thumb



RULES

{
    "rules": {
        "servers": {
            "$serverID": {
                "details": {
                    ".write": "data.parent().child('members').child(auth.uid).val() == 'owner'",
                    ".read": "data.parent().child('members').child(auth.uid).exists()",
                },
                "threads": {
                    ".read": "data.parent().child('members').child(auth.uid).exists()",
                    ".write": "data.parent().child('members').child(auth.uid).exists()",
                },
                "posts": {
                    ".read": "data.parent().child('members').child(auth.uid).exists()",
                    ".write": "data.parent().child('members').child(auth.uid).exists()",
                }
            }
        }
    }
}