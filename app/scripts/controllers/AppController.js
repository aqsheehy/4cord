import Controller from './Controller';

export default class AppController extends Controller {

    constructor(){ super();
        this.loadScript('/scripts/init-nav.js');
        this.loadScript('/scripts/init-server.js');
        this.loadScript('/scripts/init-thread.js');
        document.querySelectorAll('dialog').forEach(dialog => {
            dialogPolyfill.registerDialog(dialog);
        })
    }

}