import Controller from './Controller';
import RouterInstance from '../libs/Router';
import DatabaseInstance from '../libs/Database';
import NavView from '../views/NavView';
import FileUpload from '../libs/FileUpload';

export default class NavController extends Controller {

    constructor(){ super();
        
        this.view = new NavView(this);
        this.database = DatabaseInstance();
        this.router = RouterInstance();
        this.files = FileUpload();

        this.loadCSS('/styles/nav.css');
        this.load();

        this.router.on('/server/:name', 
            name => this.view.setServer(name),
            () => {},
            name => this.view.setServer(name));
    }

    async load(){
    
        var serverNode = await this.database.myServers();

        serverNode.on('value', servers => {
            this.view.unloadServers();
            if (servers = servers.val())
                this.view.loadServers(servers);
        });

    }
}