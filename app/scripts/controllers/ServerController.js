import Controller from './Controller';
import RouterInstance from '../libs/Router';
import DatabaseInstance from '../libs/Database';
import FileUpload from '../libs/FileUpload';
import ServerView from '../views/ServerView';

export default class ServerController extends Controller {

    constructor() { super();        

        this.view = new ServerView(this);
        this.database = DatabaseInstance();
        this.router = RouterInstance();
        this.files = FileUpload();
        this.unload();

        this.router.on('/server/:name', 
            name => this.load(name),
            () => this.unload(),
            name => this.load(name));
    }

    async load(server) {
        if (!this.currentServer) this.loadCSS('/styles/server.css');
        else if (this.currentServer === server) return;
        this.currentServer = server;
        
        var serverNode = await this.database.server(server)
        var threadsNode = await this.database.threads(server);

        serverNode.on('value', server => {
            this.view.unloadServer();
            if (server = server.val()) 
                this.view.loadServer(server);

            this.database.bookmarkServer(server);
        });

        threadsNode.on('value', threads => {
            this.view.unloadThreads();
            if (threads = threads.val()) 
                this.view.loadThreads(server, threads);
        });
    }

    unload() {
        this.currentServer = '';
        this.view.unloadThreads();
        this.view.unloadServer();
    }

}