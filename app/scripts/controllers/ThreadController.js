import Controller from './Controller';
import RouterInstance from '../libs/Router';
import DatabaseInstance from '../libs/Database';
import ThreadView from '../views/ThreadView';
import FileUpload from '../libs/FileUpload';

export default class ThreadController extends Controller {

    constructor() { super();

        this.view = new ThreadView(this);
        this.router = RouterInstance();
        this.database = DatabaseInstance();
        this.files = FileUpload();
        this.unload();

        this.router.on('/server/:name/thread/:id', 
            (name, id) => this.load(name, id),
            () => this.unload(),
            (name, id) => this.load(name, id));
    }

    async load(server, thread){

        if (!this.currentServer) this.loadCSS('/styles/thread.css');
        else if (this.currentServer === server && this.currentTopic === thread) return;

        this.view.load(server, thread);

        var threadNode = await this.database.thread(server, thread);
        var postsNode = await this.database.posts(server, thread);

        threadNode.on('value', thread => {
            this.view.unloadThread();
            if (thread = thread.val())
                this.view.loadThread(thread);
        });

        postsNode.on('value', posts => {
            this.view.unloadPosts();
            if (posts = posts.val())
                this.view.loadPosts(posts);
        });
    }

    unload(){
        this.view.unloadPosts();
        this.view.unloadThread();
    }

}