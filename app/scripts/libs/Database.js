import FirebaseInstance from './Firebase.js';

export default function DatabaseInstance() {
  if (typeof window.DatabaseInstance_ !== 'undefined')
    return window.DatabaseInstance_;

  window.DatabaseInstance_ = new Database();

  return window.DatabaseInstance_;
}

class Database {

    constructor() { this.firebase = FirebaseInstance(); }

    async uid(){
        var db = await this.firebase.database;
        return this.firebase.user.uid;
    }

    async myServers() {
        var db = await this.firebase.database;
        var uid = this.firebase.user.uid;
        return db.ref(`users/${uid}/servers`);
    }

    async isBookmarked(server){
        var db = await this.firebase.database;
        var uid = this.firebase.user.uid;
        return db.ref(`users/${uid}/servers/${server.key}`).exists();
    }

    async bookmarkServer(server) {
        var db = await this.firebase.database;
        var uid = this.firebase.user.uid;
        return db.ref(`users/${uid}/servers/${server.key}`).set(server);
    }

    async unbookmarkServer(server) {
        var db = await this.firebase.database;
        var uid = this.firebase.user.uid;
        return db.ref(`users/${uid}/servers/${server.key}`).remove();
    }

    async server(serverId) {
        var db = await this.firebase.database;
        return db.ref(`servers/${serverId}/details`);
    }

    async createServer(name, fileUrl) {
        var db = await this.firebase.database;
        var ref = db.ref(`servers`).push();
        var updates = {};
        updates[`servers/${ref.key}/details`] = { 
            author: this.firebase.user.uid,
            name: name,
            key: ref.key,
            url: fileUrl || ''
        };
        updates[`servers/${ref.key}/members/${this.firebase.user.uid}`] = 'owner';
        updates[`servers/${ref.key}/members/*`] = 'visitor';
        await db.ref().update(updates);
        return updates[`servers/${ref.key}/details`];
    }

    async threads(serverId) {
        var db = await this.firebase.database;
        return db.ref(`servers/${serverId}/threads`);
    }

    async thread(serverId, threadId){
        var db = await this.firebase.database
        return db.ref(`servers/${serverId}/threads/${threadId}`);
    }

    async createThread(serverId, title, url, text) {
        var db = await this.firebase.database;
        var threadRef = db.ref(`servers/${serverId}/threads`).push();
        var postRef = db.ref(`servers/${serverId}/posts/${threadRef.key}`).push();
        var updates = {};
        updates[`servers/${serverId}/threads/${threadRef.key}`] = { 
            author: this.firebase.user.uid,
            title: title || '',
            url: url || '',
            key: threadRef.key
        };
        updates[`servers/${serverId}/posts/${threadRef.key}/${postRef.key}`] = { 
            author: this.firebase.user.uid,
            text: text || '',
            url: url || '',
            key: postRef.key
        };
        await db.ref().update(updates);
        return threadRef.key;
    }

    async posts(serverId, threadId) {
        var db = await this.firebase.database;
        return db.ref(`servers/${serverId}/posts/${threadId}`);
    }

    async createPost(serverId, threadId, url, text) {
        var db = await this.firebase.database;
        var ref = db.ref(`servers/${serverId}/posts/${threadId}`).push();
        var updates = {};
        updates[`servers/${serverId}/posts/${threadId}/${ref.key}`] = { 
            author: this.firebase.user.uid,
            text: text,
            url: url,
            key: ref.key
        };
        await db.ref().update(updates);
        return ref.key;
    }

}