const FirebaseConfig = {
    apiKey: "AIzaSyCR5aPCJ-8Mdfq6ULXen2oin2qCbRFZZs4",
    authDomain: "test-dc65a.firebaseapp.com",
    databaseURL: "https://test-dc65a.firebaseio.com",
    projectId: "test-dc65a",
    storageBucket: "test-dc65a.appspot.com",
    messagingSenderId: "343504090815"
};

export default function FirebaseInstance () {

  if (typeof window.FirebaseInstance_ !== 'undefined')
    return window.FirebaseInstance_;

  window.FirebaseInstance_ = new Firebase();

  return window.FirebaseInstance_;
  
}

class Firebase {

    constructor() {
        firebase.initializeApp(FirebaseConfig);
        this.auth = firebase.auth();
    }

    login() {
        return new Promise((resolve, reject) => {
            if (this.user) return resolve(this.user);

            this.auth.onAuthStateChanged((user) => {
                this.user = user;
                if (user) resolve(user);
            });
            this.auth.signInAnonymously().catch((error) => {
                console.error('There was an error authenticating anonymously', error);
                reject(error);
            });
        });
    }
    
    get ref() { return firebase; }
    get database() { return this.login().then(() => firebase.database()); }
    get storage() { return this.login().then(() => firebase.storage()); }
}