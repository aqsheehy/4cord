import Modal from '../libs/Modal';

export default class NavView {

    constructor(controller) {

        this.controller = controller;

        this.servers = document.querySelector('.server-list .servers');
        this.addBtn = document.querySelector('.server-list button');
        this.userImage = document.querySelector('.server-list img');

        var dialog = document.querySelector('dialog.serverDialog');
        var modal = new Modal(dialog);     

        this.addBtn.addEventListener('click', (e) => {

            modal.open().then(async (modal) => {
                var form = dialog.querySelector('form');
                var name = form.querySelector('input').value;
                var image = form.querySelector('input[type="file"]');
                
                var fileUrl = await controller.files.upload(image);
                var server = await controller.database.createServer(name, fileUrl);
                controller.router.go(`/server/${server.key}`);

                if (modal.open) modal.close();
                form.reset();
            });

        });
    }

    unloadServers(){
        this.servers.innerHTML = '';
    }

    loadServers(servers){
        servers = Object.entries(servers);

        servers
            .map(server => this.createThumb(server[1]))
            .forEach(server => this.servers.appendChild(server));
    }

    setServer(serverId) {
        this.currentServer = serverId;
        this.servers.querySelectorAll('.active').forEach(img => {
            img.classList.remove('active');
        });
        var currentServer = this.servers.querySelector(`#server_${serverId}`);
        if (currentServer)
            currentServer.classList.add('active');
    }

    createThumb(server) {
        var thumb = document.createElement('img')
        thumb.src = server.url ||  `//www.gravatar.com/avatar/${md5(server.key)}?d=retro&s=50`;
        thumb.width = 50;
        thumb.id = `server_${server.key}`;

        if (server.key == this.currentServer) 
            thumb.classList.add('active');

        thumb.addEventListener('click', (e) => {
            e.preventDefault();
            this.controller.router.go(`/server/${server.key}`);
        });
        return thumb;
    }

}