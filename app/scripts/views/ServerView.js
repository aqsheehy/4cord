import Modal from '../libs/Modal';

export default class ServerView {
    
    constructor(controller) {

        this.controller = controller;

        this.topics = document.querySelector('.topics');
        
        var serverButtons = document.querySelectorAll('nav footer button');
        this.postBtn = serverButtons[0];
        this.shareBtn = serverButtons[1];
        this.bookmarkBtn = serverButtons[2];

        this.serverHeader = document.querySelectorAll('h6')[1];
        this.topicTmpl = document.querySelector('#topicTmpl');

        var that = this;

        var dialog = document.querySelector('dialog.threadDialog');
        var modal = new Modal(dialog);

        this.postBtn.addEventListener('click', (e) => {
            modal.open().then(async (modal) => {

                var form = dialog.querySelector('form');
                var title = form.querySelector('input[name="title"]').value;
                var image = form.querySelector('input[type="file"]');
                var comment = form.querySelector('textarea').value;
                
                var fileUrl = await controller.files.upload(image);
                var ref = await controller.database.createThread(that.currentServer.key, title, fileUrl, comment);
                controller.router.go(`/server/${that.currentServer.key}/thread/${ref}`);
                
                if (modal.open) modal.close();
                form.reset();
            });
        });

        this.bookmarkBtn.addEventListener('click', (e) => {
            controller.database.unbookmarkServer(that.currentServer);
        });
    }

    loadServer(server) {
        this.currentServer = server;
        this.serverHeader.textContent = server.name;
    }

    unloadServer(){
        this.serverHeader.textContent = '';
    }

    loadThreads(serverId, threads){
        threads = Object.entries(threads);

        threads
            .map(thread => this.createThread(thread[0], thread[1]))
            .forEach(thread => this.topics.appendChild(thread));

        this.topics.querySelectorAll('.topic').forEach((topic, i) => {
            topic.addEventListener('click', e => {
                e.preventDefault();
                this.controller.router.go(`/server/${serverId}/thread/${threads[i][0]}`);
            });;
        });
    }

    unloadThreads(){
        this.topics.innerHTML = '';
    }

    createThread(key, thread) {
        this.topicTmpl.content.querySelector('img').src =  thread.url ||  `//www.gravatar.com/avatar/${md5(thread.key)}?d=retro&s=50`;
        this.topicTmpl.content.querySelector('a').textContent = thread.title || '????';
        return document.importNode(this.topicTmpl.content, true);
    }
}