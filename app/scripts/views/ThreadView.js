import Modal from '../libs/Modal';

export default class ThreadView  {

    constructor(controller) {

        this.controller = controller;
        this.thread = document.querySelector('.thread');
        this.threadHeader = document.querySelectorAll('h6')[0];
        this.replyBtn = document.querySelector('footer button');
        this.postTmpl = document.querySelector('#postTmpl');

        var dialog = document.querySelector('dialog.postDialog');
        var modal = new Modal(dialog);

        var that = this;
        this.replyBtn.addEventListener('click', (e) => {
            modal.open().then(async (modal) => {
                var form = dialog.querySelector('form');
                var image = form.querySelector('input');
                var comment = form.querySelector('textarea').value;

                var fileUrl = await controller.files.upload(image);
                var ref = await controller.database.createPost(that.currentServer, that.currentThread, fileUrl, comment);

                controller.router.go(`/server/${that.currentServer}/thread/${that.currentThread}`);
                if (modal.open) modal.close();
                form.reset();
            });
        });
    }

    load(server, thread) {
        this.currentServer = server;
        this.currentThread = thread;
    }

    loadThread(thread){
        this.threadHeader.textContent = thread.title;
    }

    unloadThread(){
        this.threadHeader.textContent = '';
    }

    loadPosts(posts) {
        posts = Object.entries(posts);

        if (posts[0])
            this.thread.appendChild(this.createOp(posts[0][1]));

        posts
            .slice(1)
            .map(post => this.thread.appendChild(this.createReply(post[1])));
    }

    unloadPosts(){
        this.thread.innerHTML = '';
    }

    createOp(post) {
        this.postTmpl.content.querySelector('.postContainer').classList.remove('replyContainer');
        this.postTmpl.content.querySelector('.postContainer').classList.add('opContainer');
        this.postTmpl.content.querySelector('.post').classList.remove('reply');
        this.postTmpl.content.querySelector('.post').classList.add('op');
        return this.createPost(post);
    }

    createReply(post) {
        this.postTmpl.content.querySelector('.postContainer').classList.add('replyContainer');
        this.postTmpl.content.querySelector('.postContainer').classList.remove('opContainer');
        this.postTmpl.content.querySelector('.post').classList.add('reply');
        this.postTmpl.content.querySelector('.post').classList.remove('op');
        return this.createPost(post);
    }

    createPost(post){
        //this.postTmpl.content.querySelector('span.name').textContent = post.author;
        this.postTmpl.content.querySelector('.posteruid span').textContent = post.author;
        this.postTmpl.content.querySelector('.postNum.desktop a:last-child').textContent = post.key;
        if (post.url) {
            this.postTmpl.content.querySelector('img').src = post.url;
            this.postTmpl.content.querySelector('.file').classList.remove('hidden');
        }
        else 
            this.postTmpl.content.querySelector('.file').classList.add('hidden');
        this.postTmpl.content.querySelector('blockquote').innerText = post.text;
        return document.importNode(this.postTmpl.content, true);   
    }
}